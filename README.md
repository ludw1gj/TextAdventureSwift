# Text Adventure Game written in Swift | 2017

Just your average text adventure game written in swift. I started this repo after listening to a lecture by
Andy Matuschak called Controlling Complexity in Swift: Making Value Types Friends. I wanted
to practice minimising mutability and to incorporate as much logic into the value layer
rather than the reference layer. Hopefully I did a decent job at this.

Here's a link to the lecture: [Controlling Complexity in Swift: Making Value Types Friends](https://academy.realm.io/posts/andy-matuschak-controlling-complexity/)
