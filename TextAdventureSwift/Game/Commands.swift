//
//  Commands.swift
//  TextAdventrueSwift
//

import Foundation

struct Command {
    let action: String
    let hotkey: String
    let function: () -> Void
}

struct Commands {
    let commands: [Command]

    init(_ commands: [Command]) {
        self.commands = commands
    }

    func displayCommands() -> String {
        let commandsHUD = commands.reduce("") { commandsHUDAccumlator, command in
            return commandsHUDAccumlator + "\(command.action) - \(command.hotkey) | "
        }
        return String(commandsHUD.dropLast(3))
    }

    func getCommand(forHotkey hotkey: String) -> Command? {
        for command in commands {
            if command.hotkey == hotkey {
                return command
            }
        }
        return nil
    }
}
