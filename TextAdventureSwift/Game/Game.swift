//
//  Game.swift
//  TextAdventrueSwift
//

import Foundation

final class Game {
    var player: Player
    var world: World

    init() {
        self.player = Player.maxwell
        self.world = World.gameWorld
    }

    func run() {
    }
}
