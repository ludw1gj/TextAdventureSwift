//
//  Direction.swift
//  TextAdventureSwift
//

import Foundation

enum Direction {
    case north
    case east
    case south
    case west

    func getDirection() -> String {
        switch self {
        case .north:
            return "North"
        case .east:
            return "East"
        case .south:
            return "South"
        case .west:
            return "West"
        }
    }
}
