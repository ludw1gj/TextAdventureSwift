//
//  Position.swift
//  TextAdventureSwift
//

import Foundation

struct Position {
    let x: Int
    let y: Int

    func describe() -> String {
        return """
        x - \(x), y -\(y)
        """
    }
}

extension Position: Equatable {
    static func ==(lhs: Position, rhs: Position) -> Bool {
        return lhs.x == rhs.x && lhs.y == rhs.y
    }
}
