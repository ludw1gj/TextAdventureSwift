//
//  Area.swift
//  TextAdventrueSwift
//

import Foundation

struct Area {
    let name: String
    let position: Position
    let descriptionIfNoEnemy: String?
    let descriptionIfEnemy: String?
    private(set) var enemy: Enemy?

    func info() -> String {
        return """
        \(name)

        \(getDescription())

        Position: \(position.describe())
        """
    }

    func getDescription() -> String {
        // TODO: Test this guard statement
        guard let enemy = enemy, enemy.isAlive() else {
            guard let description = descriptionIfNoEnemy else {
                fatalError("Area description when enemy is not present is not set.")
            }
            return description
        }

        guard let description = descriptionIfEnemy else {
            fatalError("Area description when enemy is present is not set.")
        }
        return description
    }

    mutating func battle(player: Player) {
        if enemy == nil {
            print("There is no enemy.")
            return
        }

        func attack() {
            enemy!.receiveDamage(damage: player.strength)
            if !enemy!.isAlive() {
                print("You have killed \(enemy!.name).")
                return
            }
            print("\nEnemy has sustained damage. Enemy HP:", enemy!.hp)
        }

        while player.isAlive() && enemy!.isAlive() {
            print("Attack - a")
            print("Choose Option:")

            if let hotkey = readLine() {
                if "a" == hotkey.lowercased() {
                    attack()
                }
            }
        }
    }
}

extension Area {
    static let village = Area(name: "Village", position: Position(x:0, y:0), descriptionIfNoEnemy: "Old and weary looking Village.", descriptionIfEnemy: nil, enemy: nil)

    static let forest = Area(name: "Forest", position: Position(x: 0, y: 1), descriptionIfNoEnemy: "A creepy forest, a black rabbit appears and begins to advance towards you.", descriptionIfEnemy: "Corpse of a black rabbit.", enemy: Enemy.blackRabbit)
}
