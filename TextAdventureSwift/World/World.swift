//
//  World.swift
//  TextAdventrueSwift
//

import Foundation

struct World {
    private(set) var areas: [Area]
    private(set) var currentArea: Area

    mutating func movePlayer(_ player: Player, inDirection direction: Direction) {
        updateArea(currentArea)

        guard let area = findArea(atPosition: currentArea.position, inDirection: direction) else {
            print("There is nothing to the \(direction.getDirection()).")
            return
        }
        currentArea = area

        print(currentArea.getDescription())
        if currentArea.enemy != nil {
            currentArea.battle(player: player)
        }
    }

    func showSurroundingAreas(ofPosition position: Position) -> String {
        let surroundingAreas: [Direction:Area?] = [
            Direction.north: findArea(atPosition: position, inDirection: .north),
            Direction.south: findArea(atPosition: position, inDirection: .south),
            Direction.east: findArea(atPosition: position, inDirection: .east),
            Direction.west: findArea(atPosition: position, inDirection: .west)
        ]

        let areaMap = surroundingAreas.reduce("- Map -\n") {
            (areaMapAccumulator, surroundingArea) in
            let (direction, area) = surroundingArea

            let noArea = "Nothing."

            switch direction {
            case .north:
                return areaMapAccumulator + "North:\t\(area?.getDescription() ?? noArea)\n"
            case .east:
                return areaMapAccumulator + "East:\t\(area?.getDescription() ?? noArea)\n"
            case .south:
                return areaMapAccumulator + "South:\t\(area?.getDescription() ?? noArea)\n"
            case .west:
                return areaMapAccumulator + "West:\t\(area?.getDescription() ?? noArea)\n"
            }
        }
        return areaMap
    }

    private func findArea(atPosition position: Position, inDirection direction: Direction?) -> Area? {
        func getArea(atPosition position: Position) -> Area? {
            for area in areas {
                if area.position == position {
                    return area
                }
            }
            return nil
        }

        guard let direction = direction else {
            return getArea(atPosition: position)
        }

        switch direction {
        case .north:
            return getArea(atPosition: Position(x: position.x, y: position.y-1))
        case .south:
            return getArea(atPosition: Position(x: position.x, y: position.y+1))
        case .east:
            return getArea(atPosition: Position(x: position.x-1, y: position.y))
        case .west:
            return getArea(atPosition: Position(x: position.x+1, y: position.y))
        }
    }

    private mutating func updateArea(_ updatedArea: Area) {
        for (index, area) in areas.enumerated() {
            if area.position == updatedArea.position {
                areas[index] = updatedArea
                return
            }
        }
        fatalError("Failed to find index of area.")
    }
}

extension World {
    static let gameWorld = World(areas: [Area.village, Area.forest], currentArea: Area.village)
}
