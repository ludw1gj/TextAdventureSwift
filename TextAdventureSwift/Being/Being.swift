//
//  Being.swift
//  TextAdventureSwift
//

import Foundation

protocol Being {
    var name: String { get }
    var hp: Int { get }
    var strength: Int { get }

    func isAlive() -> Bool
    func info() -> String
    mutating func receiveDamage(damage: Int)
}

extension Being {
    func isAlive() -> Bool {
        return hp != 0
    }
}
