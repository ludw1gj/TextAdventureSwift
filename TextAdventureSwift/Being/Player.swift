//
//  Player.swift
//  TextAdventrueSwift
//

import Foundation

struct Player: Being {
    let name: String
    private(set) var hp: Int
    private(set) var strength: Int
    private(set) var xp: Int

    func info() -> String {
        return """
        \(name)

        HP: \(hp)
        """
    }

    mutating func receiveDamage(damage: Int) {
        if (hp - damage) < 0 {
            hp = 0
            return
        }
        hp -= damage
    }

    mutating func gainXP(_ xpGained: Int) {
        xp += xpGained
    }
}

extension Player {
    static let maxwell = Player(name: "Maxwell", hp: 5, strength: 2, xp: 0)
}
