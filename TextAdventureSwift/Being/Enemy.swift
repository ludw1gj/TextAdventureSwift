//
//  Enemy.swift
//  TextAdventrueSwift
//

import Foundation

struct Enemy: Being {
    let name: String
    let strength: Int
    let xpFromKill: Int
    private(set) var hp: Int

    func info() -> String {
        return """
        \(name)

        HP: \(hp)
        Strength: \(strength)
        XP from Kill: \(xpFromKill)
        """
    }

    mutating func receiveDamage(damage: Int) {
        if (hp - damage) < 0 {
            hp = 0
            return
        }
        hp -= damage
    }
}

extension Enemy {
    static let blackRabbit = Enemy(name: "Black Rabbit", strength: 1, xpFromKill: 2, hp: 5)
}
