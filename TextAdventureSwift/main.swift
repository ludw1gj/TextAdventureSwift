//
//  main.swift
//  TextAdventrueSwift
//

import Foundation

func loadGame() {
    let game = Game()
    game.run()
}

loadGame()
